﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;

public enum GameColorCategory
{
    Shield,
    Pickup,
    All
}

public class GameColor
{
    public Color Color { get; set; } = new Color();
    public float Probability { get; set;} = 0f;
    public GameColorCategory Category { get; set; } = GameColorCategory.Shield;
    public string Name { get; set; } = "";
    public bool Active { get; set; } = true;
    public float NormalisedProbThreshMin { get; set; } = 0f;
    public float NormalisedProbThreshMax { get; set; } = 0f;
}

public class GameColorManager
{
    public readonly List<GameColor> gameColors = new List<GameColor>();

    public GameColor GetColor(string name)
    {
        GameColor result = gameColors.Find( o => o.Name == name);
        result = result == null ? new GameColor() : result;
        return result;
    }

    public GameColor GetAnyRandomColor()
    {
        List<GameColor> filteredList = gameColors.Where(o => o.Active == true).ToList();
        GameColor gC = GetRandomColorFromList(filteredList);
        return gC;
    }

    public GameColor GetRandomColorFromCat(GameColorCategory cat)
    {
        List<GameColor> filteredList = gameColors.Where(o => o.Category == cat && o.Active == true).ToList();
        GameColor gC = GetRandomColorFromList(filteredList);
        return gC;
    }

    private GameColor GetRandomColorFromList(List<GameColor> gCList)
    {
        UpdateProbabilities(gCList);
        float randomVal = Random.Range(0f,1f);
        GameColor result = gCList.Find( o =>    randomVal >  o.NormalisedProbThreshMin &&
                                                randomVal <= o.NormalisedProbThreshMax &&
                                                o.Active == true);
        result = result == null ? new GameColor() : result;
        return result;
    }

    public void AddColorFromHtmlString(
        string htmlColorString, 
        float probability, 
        GameColorCategory category, 
        string name,
        bool active = true)
    {
        Color currentColor = new Color();
        ColorUtility.TryParseHtmlString(htmlColorString, out currentColor); 
        AddColor(currentColor, probability, category, name, active);
  
    }

    public void AddColor(
        Color newColor, 
        float probability, 
        GameColorCategory category, 
        string name,
        bool active = true)
    {
        gameColors.Add(new GameColor{
            Color       = newColor,
            Probability = probability,
            Category    = category,
            Name        = name,
            Active      = active
        });  
    }


    public void RemoveColorFromHtmlString(string htmlColorString)
    {
        Color currentColor = new Color();
        ColorUtility.TryParseHtmlString(htmlColorString, out currentColor); 
        RemoveColor(currentColor);    
    }

    public void RemoveColor(Color newColor)
    {
        gameColors.RemoveAll(x => x.Color == newColor);
    }

    private void UpdateProbabilities(List<GameColor> listToUpdate)
    {
        float probSum = 0f;
        foreach( GameColor gC in listToUpdate)
        {
            probSum += gC.Probability;
        }

        // Assumes the default threshold values of GameColor are 0
        GameColor lastColor = new GameColor();
        float currentMaxThresh = 0f;
        for (int clrCtr = 0; clrCtr < listToUpdate.Count; clrCtr++)
        {
            currentMaxThresh += listToUpdate[clrCtr].Probability/probSum;
            listToUpdate[clrCtr].NormalisedProbThreshMin = lastColor.NormalisedProbThreshMax;
            listToUpdate[clrCtr].NormalisedProbThreshMax = currentMaxThresh;
            lastColor = listToUpdate[clrCtr];

            // // Ensure last val is 1, as it could be less due to floating point loss
            // if(clrCtr == listToUpdate.Count - 1){listToUpdate[clrCtr].NormalisedProbThreshMax = 1f;}
        }
    }
}

public static class GameManagmentProperties
{
    private static GameColorManager colorMan;
    public static GameColorManager ColorMan 
    { 
        get
        {
            if(!initialisedColors)
            {
                InitColors();
                initialisedColors = true;
            }
            return colorMan;
        } 
    }

    public static bool initialisedColors;

    private static void InitColors()
    {
        colorMan = new GameColorManager();

        colorMan.AddColorFromHtmlString("#ffa21f", 1f, GameColorCategory.Shield, "");
        colorMan.AddColorFromHtmlString("#dedede", 1f, GameColorCategory.Shield, "");
        colorMan.AddColorFromHtmlString("#fae528", 1f, GameColorCategory.Shield, "");
        colorMan.AddColorFromHtmlString("#15d128", 0.3f, GameColorCategory.Pickup, "HealthPickUp", false);
    }
    
}
