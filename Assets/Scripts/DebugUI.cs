﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public static class DebugOptions
{
    public static bool GridReport { get; set; } = false;
    public static bool GridSegmentLabels { get; set; } = false;

}


public class DebugUI : MonoBehaviour {


	#region Properties
    [SerializeField]
    private GUISkin debugGUISkin;

    [SerializeField]
	private Vector2 debugConsoleOffset;
	private Vector2 openConsoleDim;
	private Vector2 closedConsoleDim;

    [SerializeField]
    private GameObject shipGO;
    private ShipController shipController;

    [SerializeField]
    private GameObject bossGO;
    private BossController bossController;

    [SerializeField]
    private Camera TopDownCamera; 

    [SerializeField]
    private Camera ThrdPersonCamera;

    private float frameRate;
    private int framesInPeriod;
    private float periodTimeTot;
    [SerializeField]
    private float samplePeriod;

    private Vector2 scrollPosition;

    private bool debugMenuVisible;

	private Vector2 debugConsoleDim;


    #endregion


    void Awake()
    {
        shipController = shipGO.GetComponent<ShipController>();
        bossController = bossGO.GetComponent<BossController>();
        

        TopDownCamera.enabled       = false;
        ThrdPersonCamera.enabled    = true;
        
        frameRate       = 0;
        periodTimeTot   = 0;
        framesInPeriod  = 0;

        debugMenuVisible = false;

        openConsoleDim.y = Screen.width/3f;
        openConsoleDim.x = Screen.height/2f;

        closedConsoleDim.x = 75;
        closedConsoleDim.y = 170;

        debugConsoleDim = closedConsoleDim;
    }

    void Update()
    {

        if(periodTimeTot < samplePeriod)
        {
            periodTimeTot += Time.deltaTime;
            framesInPeriod++;
        }
        else
        {
            frameRate      = framesInPeriod/periodTimeTot;
            periodTimeTot  = 0;
            framesInPeriod = 0;
        }

    }

    void OnGUI()
    {
        GUI.skin = debugGUISkin;

        debugConsoleDim = debugMenuVisible ? openConsoleDim : closedConsoleDim;

        GUILayout.BeginArea (   new Rect(   debugConsoleOffset.y,
                                            debugConsoleOffset.x, 	
                                            debugConsoleDim.y, 
                                            debugConsoleDim.x), 
                                new GUIStyle("box"));
        
        // Debug menu toggle
        if (GUILayout.Button( "Debug Menu"))
        {
            debugMenuVisible = !debugMenuVisible;
        }

        if(debugMenuVisible)
        {

            scrollPosition = GUILayout.BeginScrollView(scrollPosition);

            // Camera toggle
            if (GUILayout.Button("Toggle Camera"))
            {
                TopDownCamera.enabled       = !TopDownCamera.enabled;
                ThrdPersonCamera.enabled    = !ThrdPersonCamera.enabled;
            }

            // Frame rate
            GUILayout.Label("Frame rate: " + frameRate.ToString("00.00"));

            // Boss fire toggle
            if (GUILayout.Button("Boss Attack"))
            {
                bossController.AttackManager.FiringActive = !bossController.AttackManager.FiringActive;
            }

            // Boss expand toggle
            if (GUILayout.Button("Boss Expand"))
            {
                bossController.AttackManager.ExpansionActive = !bossController.AttackManager.ExpansionActive;
            }

            // Debug seg labels toggle
            if (GUILayout.Button("Segment labels"))
            {
                DebugOptions.GridSegmentLabels = !DebugOptions.GridSegmentLabels;
            }

            // Boss grid debug report toggle
            if (GUILayout.Button("Grid debug report"))
            {
                DebugOptions.GridReport = !DebugOptions.GridReport;
            }

            // End the scrollview we began above.
            GUILayout.EndScrollView();
        }

        GUILayout.EndArea ();
	}
}
