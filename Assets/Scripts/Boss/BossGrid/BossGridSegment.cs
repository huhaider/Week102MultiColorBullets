using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public enum SegmentType
{
    Standard,
    Generator,
}

public class BossGridSegment 
{
    public event System.EventHandler<MoveCompleteEventArgs> SegmentShiftComplete;
    public event System.EventHandler<MoveCompleteEventArgs> SegmentRotateComplete;

    #region Properties

    public int StraightNo { get {return straightNo; } private set {straightNo = value;} }
    private int straightNo;

    public int LayerNo { get { return layerNo; } private set { layerNo = value; } }
    private int layerNo;

    protected BossGridSegmentController SegmentCon { get; private set; }

    public bool Populated { get; private set; }

    public float Radius { get; private set; }

    public SegmentType Type { get; private set; }


    #endregion

    #region Fields

    // Segment
    private BossGridController bossGridCon;
    private GameObject segmentPf;


    #endregion

    public static void UpdateSegments(List<BossGridSegment> segmentsToUpdate, BossGridController bossGridCon)
    {
        List<BossGridSegmentController> updatedSegmentCons = segmentsToUpdate.ConvertAll(o => o.SegmentCon);

        // Put each segcon where it should be
        foreach (BossGridSegmentController segCon in updatedSegmentCons)
        {
            bossGridCon.Layers[segCon.LayerNo][segCon.StraightNo].UpdateSegmentCon(segCon);
        }

        // Remove any segcons 
        foreach (BossGridSegment seg in segmentsToUpdate)
        {
            if ((seg.StraightNo != seg.SegmentCon.StraightNo) || (seg.LayerNo != seg.SegmentCon.LayerNo))
            {
                seg.RemoveSegmentCon();
            }
        }
    }

    public BossGridSegment
    (
        BossGridController inputBossGridCon,
        int inputStraightNo,
        int inputLayerNo,
        GameObject inputSegmentPf,
        SegmentType inputType
    )
    {
        bossGridCon     = inputBossGridCon;
        StraightNo      = inputStraightNo;
        LayerNo         = inputLayerNo;
        segmentPf       = inputSegmentPf;
        Type            = inputType;
        Radius          = inputBossGridCon.MinGridRadius + inputBossGridCon.LayerIncrement * inputLayerNo;
    }

    /// <summary>
    /// Add a segment GameObject only when the seg is empty
    /// </summary>
    public void Populate()
    {
        AddSegmentGo(Object.Instantiate(segmentPf, bossGridCon.transform));
    }

    private void AddSegmentGo(GameObject segmentToAdd)
    {
        if(!Populated)
        {
            var segCon = segmentToAdd.GetComponent<BossGridSegmentController>();
            if(!segCon.Initialized)
            {
                segCon.Init
                (
                    bossGridCon.transform,
                    bossGridCon.SegAngle,
                    Radius,
                    bossGridCon.SegAngle - bossGridCon.SegmentBuffer,
                    straightNo,
                    LayerNo,
                    bossGridCon.NoOfLayers,
                    bossGridCon.NoOfStraights
                );
            }
            AddSegmentCon(segCon);
        }
        else
        {
            Debug.Log("Tried to populate a segment with something already in it! Layer " + LayerNo + " , " + StraightNo);
        }
    }

    private void AddSegmentCon(BossGridSegmentController inputSegCon)
    {     
        SegmentCon = inputSegCon;
        if(SegmentCon != null)
        {
            SegmentCon.SegmentDestroyed      += HandleSegmentDestroyed;
            SegmentCon.SegmentShiftComplete  += HandleSegmentShiftComplete;
            SegmentCon.SegmentRotateComplete += HandleSegmentRotateComplete;
            Populated  = true;          
        }
        else
        {
            Populated = false;
        }
    }

    private void RemoveSegmentCon()
    {
        if(SegmentCon != null)
        {
            SegmentCon.SegmentDestroyed      -= HandleSegmentDestroyed;
            SegmentCon.SegmentShiftComplete  -= HandleSegmentShiftComplete;
            SegmentCon.SegmentRotateComplete -= HandleSegmentRotateComplete;
            SegmentCon = null;
        }
        Populated = false;
    }

    protected void UpdateSegmentCon(BossGridSegmentController inputSegCon)
    {
        RemoveSegmentCon();
        AddSegmentCon(inputSegCon);
    }

    /// <summary>
    /// Start the animation of the segment that ends with its position in the next layer
    /// </summary>
    public void StartShift(AddSegmentMove addSegMove)
    {
        if (Populated)
        {
            SegmentCon.StartShift(addSegMove, bossGridCon.LayerIncrement);
        }
    }

    public void StartRotate(RotationMove rotMove)
    {
        if (Populated)
        {
            SegmentCon.StartRotation(rotMove, bossGridCon.SegAngle);
        }
    }

    public string GetReportString(bool includeInstanceId)
    {
        string outputString = "C" + "(" + SegmentCon.LayerNo + "," + SegmentCon.StraightNo;
        if (includeInstanceId)
        {
            outputString += "," + SegmentCon.GetInstanceID() + ")";
        }
        else
        {
            outputString += ")";

        }
        return outputString;
    }

    #region EventHandlers

    private void HandleSegmentShiftComplete(object sender, MoveCompleteEventArgs e)
    {
        SegmentShiftComplete?.Invoke(this, e);
    }
    private void HandleSegmentRotateComplete(object sender, MoveCompleteEventArgs e)
    {
        SegmentRotateComplete.Invoke(this, e);
    }

    private void HandleSegmentDestroyed(object sender, System.EventArgs e)
    {
        RemoveSegmentCon();
    }

    #endregion
}