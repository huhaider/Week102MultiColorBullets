﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public enum GridMoveType
{
    Rotate,
    AddSegment,
}

public class GridMove
{
    public GridMove()
    {
        Id = System.Guid.NewGuid();
    }
    public System.Guid Id { get; protected set; }
    public GridMoveType Type { get; protected set; }
    public bool IsBatchMove { get; protected set; }
    public bool Periodic { get; set; }
    public float PeriodSeconds { get; set; }
    public bool Completed { get; set; }
    public List<BossGridSegment> SegsToMove { get; private set; } = new List<BossGridSegment>();
    public List<BossGridSegment> SegsMoved  { get; private set; } = new List<BossGridSegment>();
}

public class RotationMove : GridMove
{
    public RotationMove(int layerNo, int rotationDelta)
    {
        LayerNo       = layerNo;
        RotationDelta = rotationDelta;
        Type          = GridMoveType.Rotate;
    }
    public int LayerNo          { get; private set; }
    public int RotationDelta    { get; private set; }
}

public class AddSegmentMove : GridMove
{
    public AddSegmentMove(int straightNo, int shiftDelta = 1)
    {
        StraightNo = straightNo;
        ShiftDelta = shiftDelta;
        Type       = GridMoveType.AddSegment;
    }
    public int StraightNo { get; private set; }
    public int ShiftDelta { get; private set; }
}

public class BatchAddSegmentMove : GridMove
{
    public BatchAddSegmentMove(List<AddSegmentMove> inputMoveList)
    {
        BatchList   = new List<AddSegmentMove>(inputMoveList);
        Type        = GridMoveType.AddSegment;
        IsBatchMove = true;
    }
    public List<AddSegmentMove> BatchList { get; private set; }
}

public class BatchRotateMove : GridMove
{
    public BatchRotateMove(List<RotationMove> inputMoveList)
    {
        BatchList = new List<RotationMove>(inputMoveList);
        Type      = GridMoveType.Rotate;
        IsBatchMove = true;
    }
    public List<RotationMove> BatchList { get; private set; }
}

public class MoveCompleteEventArgs : System.EventArgs
{
    public GridMove Move { get; set; }
}

public class GridMoveStack
{
    public delegate void ExecuteMoveDelegate(GridMove move);

    #region Fields

    private List<GridMove> moveList = new List<GridMove>();
    private Dictionary<GridMoveType, ExecuteMoveDelegate> execMovDic 
        = new Dictionary<GridMoveType, ExecuteMoveDelegate>();
    private bool moveInProgress;
    private GridMove currentMove;

    #endregion


    public void AddMove(GridMove move)
    {
        moveList.Add(move);
    }

    public void AddDelegate(GridMoveType moveType, ExecuteMoveDelegate moveDelegate)
    {
        execMovDic.Add(moveType, moveDelegate);
    }
    public void Update()
    {
        if (moveList.Count == 0 || execMovDic.Count == 0) { return; }
        if (!moveInProgress)
        {
            currentMove = moveList[0];
            if (currentMove.IsBatchMove)
            {
                moveInProgress = true;
                switch (currentMove.Type)
                {
                    case GridMoveType.AddSegment:
                        BatchAddSegmentMove batchAddSegMove = (BatchAddSegmentMove)currentMove;
                        foreach(AddSegmentMove move in batchAddSegMove.BatchList){ ExecuteMove(move); }
                        break;
                    case GridMoveType.Rotate:
                        BatchRotateMove batchRotMove = (BatchRotateMove)currentMove;
                        foreach (RotationMove move in batchRotMove.BatchList){ ExecuteMove(move); }
                        break;
                    default:
                        break;
                }
            }
            else
            {
                moveInProgress = true;
                ExecuteMove(currentMove);
            }
        }
    }

    private void ExecuteMove(GridMove inputMove)
    {
        moveInProgress  = true;
        execMovDic[currentMove.Type](inputMove);
    }

    public void HandleMoveComplete(object sender, MoveCompleteEventArgs e)
    {
        if (currentMove.IsBatchMove)
        {
            switch (currentMove.Type)
            {
                case GridMoveType.AddSegment:
                    BatchAddSegmentMove batchAddSegMove = (BatchAddSegmentMove)currentMove;
                    if(batchAddSegMove.BatchList.All( o => o.Completed)){ break; }
                    else{ return;}
                case GridMoveType.Rotate:
                    BatchRotateMove batchRotMove = (BatchRotateMove)currentMove;
                    var debugList = batchRotMove.BatchList.Where(o => o.Completed).ToList();
                    if (batchRotMove.BatchList.All(o => o.Completed)){ break; }
                    else{ return; }
                default:
                    break;
            }

            moveInProgress = false;
            moveList.Remove(currentMove);
            currentMove = null;

        }
        else if(currentMove.Id.Equals(e.Move.Id) && e.Move.Completed)
        {
            moveInProgress = false;
            moveList.Remove(currentMove);
            currentMove = null;
        }
    }
}

