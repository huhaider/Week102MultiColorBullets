﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class RailController : MonoBehaviour
{
    private Transform centerTrans;
    public Transform CenterTrans { get {return centerTrans;}  }
    private float radius = 0f;
    public float Radius { get {return radius;} set { radius = value; } }

    private LineRenderer railLine;
    [SerializeField]
    private int circleResolution = 50;
    [SerializeField]
    private float railWidth;

    void Awake()
    {   
        railLine = GetComponent<LineRenderer>();
        railLine.startWidth = railWidth;
        railLine.endWidth   = railWidth; 
    }   

    public void Init(Transform inputCenterTransform, float initialRadius)
    {
        centerTrans = inputCenterTransform;
        radius      = initialRadius;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(centerTrans != null)
        {
            List<Vector3> circPositions = UtilityFunctions.GetLocalCirclePoints(centerTrans,
                                                                                radius,
                                                                                circleResolution);
            railLine.positionCount = circPositions.Count;
            railLine.SetPositions(circPositions.ToArray());
        }
    }
}
