﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// Names for edges of the arc as seen from the center of the arc
/// </summary>
public enum ArcComponentType
{
    TopBack     = 0,   
    TopFront    = 1, 
    TopLeft     = 2,
    TopRight    = 3, 
    BottomBack  = 4,
    BottomFront = 5,
    BottomLeft  = 6,
    BottomRight = 7,
}

public class Arc3DComponent
{
    public List<Vector3>    Points { get; set; }
    public ArcComponentType Type { get; set; }


    public Arc3DComponent(ArcComponentType inputType)
    {
        Points  = new List<Vector3>();
        Type    = inputType;
    }
}

public class MeshData
{
    public Mesh Mesh { get; set; }
    public List<Vector3> Vertices { get; set; }
    public List<int> Triangles { get; set; }
    public int VerticesOffset { get; set; }
    
    public MeshData(Mesh mesh)
    {
        Mesh            = mesh;
        Vertices        = new List<Vector3>();
        Triangles       = new List<int>();
        VerticesOffset  = 0;
    }

    public void Update()
    {
        Mesh.Clear();
        Mesh.vertices = Vertices.ToArray();
        Mesh.triangles = Triangles.ToArray();
        Mesh.RecalculateNormals();   

        // Reset properties
        Vertices.Clear();
        Triangles.Clear();
        VerticesOffset = 0;
    }

}

[System.Serializable]
public class Arc3DConfiguration
{
    [SerializeField]
    private Vector3 center;
    public Vector3 Center { get { return center; } set{ center = value; }}

    [SerializeField]
    private float radius;
    public float Radius { get { return radius; } set{ radius = value; }}

    [SerializeField]
    [Range(-360.0f, +360.0f)]
    private float arcAngle;
    public float ArcAngle { get { return arcAngle; } set{ arcAngle = value; }}

    [SerializeField]
    [Range(1, 100)]
    private int segments;
    public int Segments { get { return segments; } set{ segments = value; }}
    
    [SerializeField]
    [Range(-360.0f, +360.0f)]
    private float startAngle;
    public float StartAngle { get { return startAngle; } set{ startAngle = value; }}

    [SerializeField]
    private float width;
    public float Width { get { return width; } set{ width = value; }}

    [SerializeField]
    private float height;
    public float Height { get { return height; } set{ height = value; }}

    public float SegAngleWidth  { get { return (ArcAngle)/Segments; } }

}

/// <summary>
/// Class used to create 3D arcs.
/// Use Init to pass an empty mesh, then use UpdateMesh to populate it
/// </summary>
public class ArcSegGenerator
{

    [SerializeField]
    private Arc3DConfiguration arc3DConfig;

    public Arc3DConfiguration Arc3DConfig { get { return arc3DConfig;} set{ arc3DConfig = value;} }

    private Dictionary<ArcComponentType, Arc3DComponent> arc3DComponents;
    private MeshData meshData;

    private Vector3 meshCenter;
    /// <summary>
    /// Populated when the UpdateMesh Function is run
    /// </summary>
    /// <value></value>
    public Vector3 MeshCenter { get { return meshCenter; } }

    public void Init(Mesh inputMesh)
    {
        meshCenter  = Vector3.zero;
        meshData    = new MeshData(inputMesh);
        InitialiseLines();
    }

    public void UpdateMesh()
    {
        // Need to fill all curved lines first, then update straight
        foreach (Arc3DComponent comp in arc3DComponents.Values)
        {
            UpdateLinesCurvedComponent(comp);
        }
        foreach (Arc3DComponent comp in arc3DComponents.Values)
        {
            UpdateLinesStraightComponent(comp);
        }

        // Populate the mesh center Vector
        UpdateMeshCenter();

        if( arc3DConfig.ArcAngle > 0)
        {
            BuildMesh(true);
        }
        else
        {
            BuildMesh(false);
        }

    }

    private void UpdateMeshCenter()
    {
        float SegAngle = arc3DConfig.StartAngle + arc3DConfig.ArcAngle/2f;
        float radiusToCenter = arc3DConfig.Radius + arc3DConfig.Width/2f;

        meshCenter = new Vector3
        (
            arc3DConfig.Center.x + radiusToCenter*Mathf.Cos(Mathf.Deg2Rad*SegAngle),
            arc3DConfig.Center.y + arc3DConfig.Height/2f,
            arc3DConfig.Center.z + radiusToCenter*Mathf.Sin(Mathf.Deg2Rad*SegAngle)
        );
    }

    private void InitialiseLines()
    {
        arc3DComponents = new Dictionary<ArcComponentType, Arc3DComponent>
        {
            {ArcComponentType.TopBack    ,new Arc3DComponent(ArcComponentType.TopBack    )},
            {ArcComponentType.TopFront   ,new Arc3DComponent(ArcComponentType.TopFront   )},      
            {ArcComponentType.TopLeft    ,new Arc3DComponent(ArcComponentType.TopLeft    )},    
            {ArcComponentType.TopRight   ,new Arc3DComponent(ArcComponentType.TopRight   )},     
            {ArcComponentType.BottomBack ,new Arc3DComponent(ArcComponentType.BottomBack )},      
            {ArcComponentType.BottomFront,new Arc3DComponent(ArcComponentType.BottomFront)},    
            {ArcComponentType.BottomLeft ,new Arc3DComponent(ArcComponentType.BottomLeft )}, 
            {ArcComponentType.BottomRight,new Arc3DComponent(ArcComponentType.BottomRight)}, 
        };
    }

    private void UpdateLinesCurvedComponent(Arc3DComponent comp)
    {
        List<Vector3> points = new List<Vector3>();

        float yCoord    = 0f;
        float arcRadius = 0f;

        float SegAngle = arc3DConfig.StartAngle;

        switch (comp.Type)
        {
            case ArcComponentType.TopBack:
                yCoord = arc3DConfig.Center.y + arc3DConfig.Height;
                arcRadius = arc3DConfig.Radius;
                break;
            case ArcComponentType.TopFront:
                yCoord = arc3DConfig.Center.y + arc3DConfig.Height;
                arcRadius = arc3DConfig.Radius + arc3DConfig.Width;
                break;
            case ArcComponentType.BottomBack:
                yCoord = arc3DConfig.Center.y;
                arcRadius = arc3DConfig.Radius;
                break;
            case ArcComponentType.BottomFront:
                yCoord = arc3DConfig.Center.y;
                arcRadius = arc3DConfig.Radius + arc3DConfig.Width;
                break;
            default:
                return;
        }

        for (int SegCtr = 0; SegCtr <= arc3DConfig.Segments; SegCtr++)
        {
            if(SegCtr != 0)
            {
                SegAngle  +=  arc3DConfig.SegAngleWidth;
            }

            points.Add(
                new Vector3(
                    arc3DConfig.Center.x + arcRadius*Mathf.Cos(Mathf.Deg2Rad*SegAngle),
                    yCoord,
                    arc3DConfig.Center.z + arcRadius*Mathf.Sin(Mathf.Deg2Rad*SegAngle)
                )
            );
        }

        comp.Points = points;
    }

    private void UpdateLinesStraightComponent(Arc3DComponent comp)
    {
        List<Vector3> points = new List<Vector3>();

        switch(comp.Type)
        {
            case ArcComponentType.TopLeft:
                points.Add(arc3DComponents[ArcComponentType.TopBack].Points.Last());
                points.Add(arc3DComponents[ArcComponentType.TopFront].Points.Last());
                break;
            case ArcComponentType.TopRight:
                points.Add(arc3DComponents[ArcComponentType.TopFront].Points[0]);
                points.Add(arc3DComponents[ArcComponentType.TopBack].Points[0]);
                break;
            case ArcComponentType.BottomLeft:
                points.Add(arc3DComponents[ArcComponentType.BottomBack].Points.Last());
                points.Add(arc3DComponents[ArcComponentType.BottomFront].Points.Last());
                break;
            case ArcComponentType.BottomRight:
                points.Add(arc3DComponents[ArcComponentType.BottomFront].Points[0]);
                points.Add(arc3DComponents[ArcComponentType.BottomBack].Points[0]);
                break;
            default:
                return;
        }

        comp.Points = points;
    }

    /// <summary>
    /// Puts together the faces from the constituent lines
    /// </summary>
    /// <param name="clockWiseAroundY"></param>
    private void BuildMesh(bool clockWiseAroundY)
    {
        // Top Face
        UpdateMeshFromLines(arc3DComponents[ArcComponentType.TopFront].Points,
                            arc3DComponents[ArcComponentType.TopBack].Points,
                            clockWiseAroundY);
        // Back Face
        UpdateMeshFromLines(arc3DComponents[ArcComponentType.TopBack].Points,
                            arc3DComponents[ArcComponentType.BottomBack].Points,
                            clockWiseAroundY);
        // Bottom Face
        UpdateMeshFromLines(arc3DComponents[ArcComponentType.BottomBack].Points,
                            arc3DComponents[ArcComponentType.BottomFront].Points,
                            clockWiseAroundY);
        // Front Face - Opposite direction to the others
        UpdateMeshFromLines(arc3DComponents[ArcComponentType.TopFront].Points,
                            arc3DComponents[ArcComponentType.BottomFront].Points,
                            !clockWiseAroundY);
        // Left Face
        UpdateMeshFromLines(arc3DComponents[ArcComponentType.TopLeft].Points,
                            arc3DComponents[ArcComponentType.BottomLeft].Points,
                            clockWiseAroundY);
        // Right Face
        UpdateMeshFromLines(arc3DComponents[ArcComponentType.TopRight].Points,
                            arc3DComponents[ArcComponentType.BottomRight].Points,
                            clockWiseAroundY);

        meshData.Update();
    }

    /// <summary>
    /// Pass in the top and bottom lines of any face of the arc to update the mesh.
    /// </summary>
    /// <param name="topLine"></param>
    /// <param name="bottomLine"></param>
    /// <param name="clockWiseAroundY"> 
    /// True when the points of the lines progress clockwise direction around the Y axis. Top down where Y is up, X is right)
    /// </param>
    private void UpdateMeshFromLines(List<Vector3> topLine, List<Vector3> bottomLine, bool clockWiseAroundY)
    {
        int noOfSeg = topLine.Count() - 1;
        
        // Bail out if there is a mismatch in number of segments
        if(noOfSeg != bottomLine.Count() - 1) { return;}

        // Update the mesh with the two input lines
        meshData.Vertices.AddRange(topLine);
        meshData.Vertices.AddRange(bottomLine);

        for (int arcSegCtr = 0; arcSegCtr < noOfSeg; arcSegCtr++)
        {
            if (clockWiseAroundY)
            {
                meshData.Triangles.Add(meshData.VerticesOffset + arcSegCtr);
                meshData.Triangles.Add(meshData.VerticesOffset + arcSegCtr + noOfSeg + 1);
                meshData.Triangles.Add(meshData.VerticesOffset + arcSegCtr + noOfSeg + 2);
                meshData.Triangles.Add(meshData.VerticesOffset + arcSegCtr);
                meshData.Triangles.Add(meshData.VerticesOffset + arcSegCtr + noOfSeg + 2);
                meshData.Triangles.Add(meshData.VerticesOffset + arcSegCtr + 1);
            }
            else
            {
                meshData.Triangles.Add(meshData.VerticesOffset + arcSegCtr);
                meshData.Triangles.Add(meshData.VerticesOffset + arcSegCtr + noOfSeg + 2);
                meshData.Triangles.Add(meshData.VerticesOffset + arcSegCtr + noOfSeg + 1);
                meshData.Triangles.Add(meshData.VerticesOffset + arcSegCtr);
                meshData.Triangles.Add(meshData.VerticesOffset + arcSegCtr + 1);
                meshData.Triangles.Add(meshData.VerticesOffset + arcSegCtr + noOfSeg + 2);
            }
        }

        meshData.VerticesOffset += topLine.Count() + bottomLine.Count();
    }

}
