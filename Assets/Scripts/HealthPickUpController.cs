﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickUpController : MonoBehaviour
{
    [SerializeField]
    private Vector3 rotationVector;

    [SerializeField]
    private int healthValue;
    public int HealthValue { get {return healthValue;} }

    /// Where the health pickup needs to end up
    private Vector3 targetPosition;

    private Vector3 startPosition;
    private float totalDistance;
    
    private Rigidbody rBody;

    [SerializeField]
    private float speed;

    private float elapsedTime;
    private Color matColor;
    public Color MatColor { get {return matColor;} }
    private bool isMoving;



    // Start is called before the first frame update
    void Awake()
    {
        rBody = GetComponent<Rigidbody>();
        GetComponent<Renderer>().material.color = GameManagmentProperties.ColorMan.GetColor("HealthPickUp").Color;
    }

    public void StartMove(Vector3 targetPosition)
    {
        startPosition       = transform.position;
        this.targetPosition = targetPosition;
        elapsedTime         = 0f;
        totalDistance       = Vector3.Distance(startPosition, this.targetPosition);
        isMoving            = true;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rBody.MoveRotation( transform.rotation * Quaternion.Euler(rotationVector));

        if(isMoving)
        {
            elapsedTime         += Time.fixedDeltaTime;
            float ratioComplete = Mathf.Clamp((elapsedTime * speed)/totalDistance,0f,1f);
            rBody.MovePosition(Vector3.Lerp(transform.position, targetPosition, ratioComplete));
            if(ratioComplete == 1){ isMoving = false; }
        }


    }


    private void OnTriggerEnter (Collider other)
    {
        GameObject otherGO = other.gameObject;

        switch (otherGO.tag)
        {
            case "Player":
                otherGO.GetComponentInParent<ShipController>().TakeHealth(healthValue);
                Destroy(this.gameObject);
                break;
            default:
                break;
        }
    }
}
