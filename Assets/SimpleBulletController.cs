﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleBulletController : MonoBehaviour
{
    [SerializeField]
    private float deltaPosition;
    public float DeltaPosition { get { return deltaPosition; } set { deltaPosition = value; } }



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        transform.Translate(new Vector3(0,0, deltaPosition), Space.Self);
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Hit something");
    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            Debug.Log("Hit player");
            AudioManager.instance.Play("ShieldDamage");
        }
    }
}
